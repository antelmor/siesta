# ---
# Copyright (C) 1996-2021       The SIESTA group
#  This file is distributed under the terms of the
#  GNU General Public License: see COPYING in the top directory
#  or http://www.gnu.org/copyleft/gpl.txt .
# See Docs/Contributors.txt for a list of contributors.
# ---
#
# The VPATH directive below allows to re-use m_getopts.f90 from the top Src.
# Other points to note, until we switch to a better building system:
#
#  The arch.make file is supposed to be in $(OBJDIR). This is normally
#  the top Obj, but if you are using architecture-dependent build directories
#  you might want to change this. (If you do not understand this, you do not
#  need to change anything. Power users can do "make OBJDIR=Whatever".)
#
#  If your main Siesta build used an mpi compiler, you might need to
#  define an FC_SERIAL symbol in your top arch.make, to avoid linking
#  in the mpi libraries even if we explicitly undefine MPI below.
#  
.SUFFIXES:
.SUFFIXES: .f .f90 .F .F90 .o

PROGS:= mprop fat dm_creator spin_texture
default: $(PROGS)

override WITH_MPI=

TOPDIR=.
MAIN_OBJDIR=.

VPATH=$(TOPDIR)/Util/COOP:$(TOPDIR)/Src

include $(MAIN_OBJDIR)/arch.make
include $(MAIN_OBJDIR)/check_for_build_mk.mk

FC_DEFAULT:=$(FC)
FC_SERIAL?=$(FC_DEFAULT)
FC:=$(FC_SERIAL)         # Make it non-recursive

INCFLAGS:=$(INCFLAGS) $(NETCDF_INCFLAGS)
INCFLAGS += $(LIBSYS_INCFLAGS)

# Two ways of splitting the list of object files:
# 1. By the needs of the three different programs
SIESTA_OBJS = precision.o alloc.o parallel.o m_io.o pxf.o moreParallelSubs.o 
DEF_OBJS = m_getopts.o io.o subs.o units.o
DEF_OBJS += io_hs.o
DEF_OBJS += main_vars.o

# Create objects for each routine
MPROP_OBJS = $(SIESTA_OBJS) $(DEF_OBJS) read_curves.o orbital_set.o  
FAT_OBJS   = $(SIESTA_OBJS) $(DEF_OBJS) read_curves.o orbital_set.o  
DM_OBJS    = $(SIESTA_OBJS) $(DEF_OBJS) iodm_netcdf.o write_dm.o  
TEXTURE_OBJS  = $(SIESTA_OBJS) $(DEF_OBJS) 

# 2. By whether they are in VPATH or in the local directory (for deps processing)
DEP_OBJS = $(SIESTA_OBJS)
DEP_OBJS += m_getopts.o io.o  units.o iodm_netcdf.o write_dm.o
LOCAL_OBJS= subs.o io_hs.o main_vars.o read_curves.o orbital_set.o  \
            mprop.o fat.o dm_creator.o spin_texture.o

# Dependencies
.PHONY: dep
dep:
	@sfmakedepend --depend=obj --modext=o \
		$(addprefix $(TOPDIR)/Src/,$(DEP_OBJS:.o=.f) $(DEP_OBJS:.o=.f90)) \
		$(addprefix $(TOPDIR)/Src/,$(DEP_OBJS:.o=.F) $(DEP_OBJS:.o=.F90)) \
		$(addprefix $(TOPDIR)/Util/COOP/,$(LOCAL_OBJS:.o=.f90) $(LOCAL_OBJS:.o=.F90)) \
		$(addprefix $(TOPDIR)/Util/COOP/,$(LOCAL_OBJS:.o=.f) $(LOCAL_OBJS:.o=.F)) || true

mprop fat dm_creator spin_texture: $(LIBSYS)

mprop: $(MPROP_OBJS) mprop.o
	$(FC) -o $@ $(LDFLAGS) $(MPROP_OBJS) mprop.o $(LIBSYS) 
fat: $(FAT_OBJS) fat.o
	$(FC) -o $@ $(LDFLAGS) $(FAT_OBJS) fat.o $(LIBSYS) 
dm_creator: $(DM_OBJS) dm_creator.o
	$(FC) -o $@ $(LDFLAGS) $(DM_OBJS) dm_creator.o $(LIBSYS) $(NETCDF_LIBS)
spin_texture: $(TEXTURE_OBJS) $(LIBSYS) spin_texture.o 
	$(FC) -o $@ $(LDFLAGS) $(TEXTURE_OBJS) spin_texture.o $(LIBSYS)

clean:
	rm -f *.o *.mod mprop dm_creator fat spin_texture

install: $(PROGS)
	cp -p $(PROGS) $(SIESTA_INSTALL_DIRECTORY)/bin

# DO NOT DELETE THIS LINE - used by make depend
io.o: m_io.o
iodm_netcdf.o: alloc.o parallel.o precision.o 
moreParallelSubs.o: alloc.o m_io.o parallel.o precision.o 
units.o: precision.o
dm_creator.o: io_hs.o iodm_netcdf.o main_vars.o subs.o
fat.o: io_hs.o main_vars.o orbital_set.o read_curves.o
io_hs.o: main_vars.o precision.o
main_vars.o: m_getopts.o precision.o subs.o units.o
mprop.o: io_hs.o main_vars.o orbital_set.o read_curves.o subs.o
orbital_set.o: main_vars.o subs.o
read_curves.o: orbital_set.o precision.o
spin_texture.o: io_hs.o main_vars.o subs.o
subs.o: precision.o
