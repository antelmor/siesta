# -----------------------------------------------------------------------------
#
#

SystemName          GaAs bulk with test of various types of WFS output 
SystemLabel         gaas_fatbands

NumberOfAtoms       2
NumberOfSpecies     2

%block ChemicalSpeciesLabel
 1  31  Ga
 2  33  As
%endblock ChemicalSpeciesLabel

PAO.BasisSize       DZP
PAO.EnergyShift     300 meV

LatticeConstant    5.653 Ang
%block LatticeVectors
  0.500  0.500  0.000
  0.000  0.500  0.500
  0.500  0.000  0.500
%endblock LatticeVectors

MeshCutoff          90.0 Ry

DM.MixingWeight      0.3 
DM.NumberPulay       3  
DM.Tolerance         1.d-4
 
kgridcutoff          7. Ang

SolutionMethod       diagon    
ElectronicTemperature  25 meV  

#
# Note: pseudo-dojo pseudos have the 3d orbitals in the valence
# Hence, the first 10 bands are d-like
#
BandLinesScale  pi/a
WFS.Write.For.Bands T             # For fat-bands analysis
Wfs.band.min 11
Wfs.band.max 18
%block BandLines                  # These are comments
 1  0.000  0.000  0.000  \Gamma   # Begin at Gamma
25  2.000  0.000  0.000     X     # 25 points from Gamma to X
10  2.000  1.000  0.000     W     # 10 points from X to W
15  1.000  1.000  1.000     L     # 15 points from W to L
20  0.000  0.000  0.000  \Gamma   # 20 points from L to Gamma
25  1.500  1.500  1.500     K     # 25 points from Gamma to K
%endblock BandLines

WaveFuncKPointsScale  pi/a
%block WaveFuncKPoints              # These are comments
0.000  0.000  0.000  from 11 to 18   # eigenstates 1-10 of Gamma
2.000  0.000  0.000  from 11 to 18   #  X
1.500  1.500  1.500  from 11 to 18   #  K
%endblock WaveFuncKPoints

COOP.Write T

AtomicCoordinatesFormat  Fractional
%block AtomicCoordinatesAndAtomicSpecies
    0.    0.    0.     1  Ga        1
    0.25  0.25  0.25   2  As        2
%endblock AtomicCoordinatesAndAtomicSpecies
